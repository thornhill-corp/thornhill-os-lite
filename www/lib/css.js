export default componentUrl => {
    const cssElement = document.createElement('link');
    cssElement.setAttribute('rel', 'stylesheet');
    cssElement.setAttribute('href', componentUrl.replace('.js', '.css'));
    document.head.appendChild(cssElement);
};