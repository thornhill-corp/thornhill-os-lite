export const enableFullscreen = async () => {
    if(!await __TAURI__.window.appWindow.isFullscreen())
        await __TAURI__.window.appWindow.setFullscreen(true);
};

export const exit = () => __TAURI__.window.appWindow.close();

export const getAppVersion = __TAURI__.app.getVersion;