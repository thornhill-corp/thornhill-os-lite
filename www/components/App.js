import { createApp } from '../assets/vue.esm-browser.js';

import css from '../lib/css.js';

import Terminal from './Terminal/Terminal.js';
import TerminalMainEntry from './Terminal/TerminalMainEntry.js';

css(import.meta.url);

const app = createApp({
    'data': () => ({
        isTerminalOpen: false
    }),
    'methods': {
        keydown: function(event){
            if(!this.isTerminalOpen && event.code === 'Space')
                event.preventDefault();
        },
        keyup: function(event){
            switch(event.code){
                case 'Space': {
                    if(!this.isTerminalOpen)
                        this.isTerminalOpen = true;
                    break;
                }
                case 'Escape': {
                    if(this.isTerminalOpen)
                        this.isTerminalOpen = false;
                    break;
                }
                case 'F5': {
                    window.location.reload();
                }
            }
        }
    },
    'mounted': function(){
        window.addEventListener('keydown', this.keydown);
        window.addEventListener('keyup', this.keyup);
    },
    'beforeUnmount': function(){
        window.removeEventListener('keydown', this.keydown);
        window.removeEventListener('keyup', this.keyup);
    },
    'template': `
        <Terminal v-if="isTerminalOpen" />
    `
});

app.component('Terminal', Terminal);
app.component('TerminalMainEntry', TerminalMainEntry);

export default app;