import css from '../../lib/css.js';
import terminal from '../../lib/terminal.js';

css(import.meta.url);

const history = [];

export default {
    'data': () => ({
        entries: [],
        isMounted: false,
        mainRef: undefined
    }),
    'computed': {
        history: () => history
    },
    'methods': {
        focus: async function(){
            // TODO prevent when selection
            await this.$nextTick();
            this.entries[this.entries.length - 1].inputRef.$el.focus();
        },
        scrollToBottom: function(){
            this.mainRef.scrollTop = this.mainRef.scrollHeight;
        },
        newline: async function(){
            this.entries.push({ value: '' });
            this.scrollToBottom();
            await this.focus();
        },
        parse: async function(command){
            const
                isComment = command.startsWith('#'),
                isBackground = command.endsWith(' &');
            if(isComment || isBackground){
                this.history.push('');
                await this.newline();
            }
            if(!isComment){
                const value = await terminal(
                    isBackground
                        ? command.slice(0, -2)
                        : command
                );
                if(!this.isMounted) return;
                if(value)
                    this.entries.push({ value, isReadOnly: true });
                this.history.push('');
                await this.newline();
            }
        },
        clear: async function(){
            this.entries.splice(0, this.entries.length);
            await this.$nextTick();
            this.entries.push({ value: this.history[this.history.length - 1] });
            await this.focus();
        }
    },
    'mounted': async function(){
        this.isMounted = true;
        if(this.history[this.history.length - 1] !== '')
            this.history.push('');
        await this.newline();
    },
    'beforeUnmount': function(){
        this.isMounted = false;
    },
    'template': `
        <div class="Terminal" @click="focus">
            <header class="Terminal__Titlebar">
                <img class="Terminal__Titlebar__Icon" src="/assets/terminal__titlebar__icon.png" alt="">
            </header>
            <main class="Terminal__Main" :ref="element => $data.mainRef = element">
                <TerminalMainEntry
                    v-for="entry in entries"
                    v-bind="entry"
                    :history="history"
                    @submit="parse"
                    @clear="clear"
                    @cancel="newline"
                    :ref="element => entry.inputRef = element"
                />
		    </main>
        </div>
    `
};