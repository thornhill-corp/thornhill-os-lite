import css from '../../lib/css.js';

css(import.meta.url);

export default {
    'props': {
        inputRef: undefined,
        value: String,
        isReadOnly: Boolean,
        history: Array
    },
    'data': function(){
        return {
            input: this.value,
            isActuallyReadOnly: this.isReadOnly,
            historyIndex: this.history.length - 1
        }
    },
    'methods': {
        updateHeight: async function(){
            await this.$nextTick();
        },
        keydown: async function(event){
            if(event.ctrlKey){
                switch(event.code){
                    case 'KeyL': {
                        this.$emit('clear');
                        break;
                    }
                    case 'KeyC': {
                        // TODO prevent when selection
                        this.isActuallyReadOnly = true;
                        this.history[this.history.length - 1] = '';
                        this.$emit('cancel');
                        break;
                    }
                }
                return;
            }
            else {
                switch(event.code){
                    case 'Enter':
                    case 'NumpadEnter': {
                        event.preventDefault();
                        if(!this.input) return;
                        this.isActuallyReadOnly = true;
                        this.history[this.history.length - 1] = this.input;
                        this.$emit('submit', this.input);
                        break;
                    }
                    case 'ArrowUp': {
                        event.preventDefault();
                        if(this.historyIndex === 0) return;
                        this.input = this.history[--this.historyIndex];
                        break;
                    }
                    case 'ArrowDown': {
                        event.preventDefault();
                        if(this.historyIndex === this.history.length - 1) return;
                        this.input = this.history[++this.historyIndex];
                        break;
                    }
                }
            }
            await this.updateHeight();
        },
        keyup: async function(event){
            if(event.code.endsWith('Enter')) return event.preventDefault();
            this.history[this.historyIndex] = this.input;
            await this.updateHeight();
        }
    },
    'mounted': async function(){
        await this.updateHeight();
    },
    'template': `
        <label class="Terminal__Main__Entry">
            <span class="Terminal__Main__Entry__Prompt">
                {{ isReadOnly ? '>' : '$' }}
            </span>
            <textarea
                class="Terminal__Main__Entry__Input"
                :readonly="isActuallyReadOnly"
                v-model="input"
                @keydown="keydown"
                @keyup="keyup"
            />
        </label>
    `
};